import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  BlockEditorProvider,
  BlockList,
  WritingFlow,
  ObserveTyping,
  Popover
} from '@wordpress/block-editor';
import { useState } from '@wordpress/element';
import { registerCoreBlocks } from '@wordpress/block-library';

registerCoreBlocks();

function MyEditorComponent() {
  const [blocks, updateBlocks] = useState([]);

  return (
    <BlockEditorProvider
      value={blocks}
      onInput={updateBlocks}
      onChange={updateBlocks}
    >
      <WritingFlow>
        <ObserveTyping>
          <BlockList />
        </ObserveTyping>
      </WritingFlow>
      {/* <Popover.Slot /> */}
    </BlockEditorProvider>
  );
}

// Make sure to load the block editor stylesheets too
import '@wordpress/components/build-style/style.css';
import '@wordpress/block-editor/build-style/style.css';
import '@wordpress/block-library/build-style/style.css';
import '@wordpress/block-library/build-style/editor.css';
import '@wordpress/block-library/build-style/theme.css';

class App extends Component {
  render() {
    return (
      // 'Hello'
      <MyEditorComponent />
    )
  }
}

const wrapper = document.getElementById("container");
ReactDOM.render(<App />, wrapper);
